// 1. Дана строка 'aaa@bbb@ccc'. Замените все @ на ! с помощью глобального
// поиска и замены.

let str='aaa@bbb@ccc';
console.log(str.replace( /@/g, '!' ));

// 2. В переменной date лежит дата в формате 2025-12-31. Преобразуйте эту
// дату в формат 31/12/2025.

let date='2025-12-31';
let newDate=date.replace(/(2025)(-)(12)(-)(31)/,'$5$4$3$2$1')
console.log(newDate.replace(/-/g, '/' ));

// 3. Дана строка «Я учу javascript!». Вырежете из нее слово «учу» и слово
// «javascript» тремя разными способами (через substr, substring, slice).

let strSub='Я учу javascript!';

console.log(strSub.substr(2, 17));

console.log(strSub.substring(2, 17));

console.log(strSub.slice(-15));

// 4. Дан массив с элементами 4, 2, 5, 19, 13, 0, 10. Найдите квадратный корень
// из суммы кубов его элементов. Для решения воспользуйтесь циклом for.

let arr=[4, 2, 5, 19, 13, 0, 10];
let sum=0;
for(i=0;i<arr.length;i++){
sum+= Math.pow(arr[i],3); 
}
console.log(Math.sqrt(sum));

// 5. Даны переменные a и b. Отнимите от a переменную b и результат
// присвойте переменной c. Сделайте так, чтобы в любом случае в переменную
// c записалось положительное значение. Проверьте работу скрипта при a и b,
// равных соответственно 3 и 5, 6 и 1.

let a=3;
let b=5;
let c=a-b;

console.log(Math.abs(c));


// 6. Выведите на экран текущую дату-время в формате 12:59:59 31.12.2014.
// Для решения этой задачи напишите функцию, которая будет добавлять 0
// перед днями и месяцами, которые состоят из одной цифры (из 1.9.2014
// сделает 01.09.2014).

let dateNow= new Date();

const getZero=(num)=>{
    if (num>0 && num<10){
        return '0' + num;
    }else{
        return num;
    }
}

console.log( `${dateNow.getHours()}:${getZero(dateNow.getMinutes())}:${dateNow.getSeconds()}
 ${getZero(dateNow.getDate())}.${getZero(dateNow.getMonth() + 1)}.${dateNow.getFullYear()} `);

// 7. Дана строка 'aa aba abba abbba abca abea'. Напишите регулярку, которая
// найдет строки aba, abba, abbba по шаблону: буква 'a', буква 'b' любое
// количество раз, буква 'a'.

let strAba='aa aba abba abbba abca abea';

console.log(strAba.match(/a{1,2}b/g));


// 8. Напишите ф-цию строгой проверки ввода номер телефона в
// международном формате (<код страны> <код города или сети> <номер
// телефона>). Функция должна возвращать true или false. Используйте
// регулярные выражения.

let telNumber='+375441220088';
let re=/^(\+375|80)(29|25|44|33)(\d{3})(\d{2})(\d{2})$/;

const numberTest=(number)=>console.log(re.test(number));

numberTest(telNumber);


// 9. Напишите ф-цию строгой проверки адреса эл. почты с учетом следующих
// условия:
// - весь адрес не должен содержать русские буквы и спецсимволы, кроме
// одной «собачки», знака подчеркивания, дефиса и точки;
// - имя эл. почты (до знака @) должно быть длиной более 2 символов, причем
// имя может содержать только буквы, цифры, но не быть первыми и
// единственными в имени;
// - после последней точки и после @, домен верхнего уровня (ru, by, com и т.п.)
// не может быть длиной менее 2 и более 11 символов.
// Функция должна возвращать true или false. Используйте регулярные
// выражения.

let email='iq@yndex.ru'

let reg = /^([A-Za-z0-9_\-\.]{2,})\@(([A-Za-z0-9_\-\.])+\.([A-Za-z]){2,11})$/;


const emailTest=(emailadress)=>console.log(reg.test(emailadress));

emailTest(email);


// 10. Напишите ф-цию, которая из полного адреса с параметрами и без,
// например: https://tech.onliner.by/2018/04/26/smart-do-200/?
// utm_source=main_tile&utm_medium=smartdo200#zag3 , получит адрес
// доменного имени (https://tech.onliner.by), остальную часть адреса без
// параметров (/2018/04/26/smart-do-200/), параметры
// (utm_source=main_tile&utm_medium=smartdo200) и хеш (#zag3). В адресе
// может и не быть каких-либо составляющих. Ф-ция должна возвращать
// массив.

let url='https://tech.onliner.by/2018/04/26/smart-do-200/?utm_source=main_tile&utm_medium=smartdo200#zag3'


const adsressName=(domen,parametrs,secondparametrs,hash)=>{
    if (domen.match(/^https?:\/\/([a-z\.]{2,})/)){
        console.log(domen.match(/^https?:\/\/([a-z\.]{2,})/));
    } 
    }
   


adsressName(url,url,url,url)



 